import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import Axios from "axios";

Vue.use(BootstrapVue);
Vue.prototype.$axios = Axios;

import App from "./App.vue";

new Vue({
    render: h => h(App)
}).$mount("#app");
