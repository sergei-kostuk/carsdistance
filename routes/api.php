<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('version', 'ApiController@version');
Route::get('cars', 'ApiController@getCars');
Route::get('distance/{imei}/{dateStart}/{dateEnd}', 'ApiController@getDistance')
    ->where(['imei' => '[0-9]+', 'dateStart' => '[0-9]+', 'dateEnd' => '[0-9]+']);
