<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Spot;
use Illuminate\Http\JsonResponse;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function version(): JsonResponse
    {
        return response()->json(['version' => '0.1-dev']);
    }

    /**
     * @return JsonResponse
     */
    public function getCars(): JsonResponse
    {
        $cars = Car::all();

        $result = [];
        foreach ($cars as $car) {
            $result[] = [
                'imei' => $car['tracker']['imei'],
                'manufacturer' => $car['manufacturer'],
                'model' => $car['model'],
                'plate_nr' => $car['plate_nr'],
            ];
        }

        return response()->json($result);
    }

    /**
     * @param string $imei
     * @param int $dateStart
     * @param int $dateEnd
     * @return JsonResponse
     * @throws \Exception
     */
    public function getDistance(string $imei, int $dateStart, int $dateEnd): JsonResponse
    {
        $spots = Spot::where('imei', $imei)
            ->where('loc', 'exists', true)
            ->where('time', '>', new \DateTime("@$dateStart"))
            ->where('time', '<', new \DateTime("@$dateEnd"))
            ->orderBy('time', 'asc')
            ->get();

        $distance = 0;
        for ($n = 0; $n < count($spots) - 1; $n += 1) {
            $distance += $this->calculateDistance(
                $spots[$n]['loc'][0],
                $spots[$n]['loc'][1],
                $spots[$n + 1]['loc'][0],
                $spots[$n + 1]['loc'][1]
            );
        }

        return response()->json(['distance' => $distance]);
    }

    /**
     * @param float $aLat
     * @param float $aLon
     * @param float $bLat
     * @param float $bLon
     * @return int
     */
    private function calculateDistance(float $aLat, float $aLon, float $bLat, float $bLon): int
    {
        $earthRadius = 6372795;

        $latFrom = deg2rad($aLat);
        $lonFrom = deg2rad($aLon);
        $latTo = deg2rad($bLat);
        $lonTo = deg2rad($bLon);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return round($angle * $earthRadius);
    }
}
