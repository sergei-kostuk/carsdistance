<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Spot extends Model
{
    protected $collection = 'spot';
    protected $dates = ['time'];
}
